import React, { useState } from 'react';
import { TaskList } from './TaskList';
import { AddTodoForm } from './AddTodoForm';

const initialTasks: Task[] = [
  {
    text: 'tarea 1',
    complete: false,
  },
  {
    text: 'tarea 2',
    complete: false,
  },
  {
    text: 'tarea 3',
    complete: true,
  },
];


function App() {
  const [tasks, setTasks] = useState(initialTasks);

  const toggleTask = (selectedTask: Task) => {
    const newTasks = tasks.map((task) => {
      if (task === selectedTask) {
        return {
          ...task, 
          complete: !task.complete, 
        };
      }
      return task;
    }); 
    setTasks(newTasks);
  };

  const addTask: AddTask = (text: string) => {
    const newTask = { text, complete: false};
    setTasks([...tasks, newTask]);
  };

  return (
    <>
      <AddTodoForm addTask={addTask} />
      <TaskList tasks={tasks} toggleTask={toggleTask} />
    </>
  );
}

export default App;