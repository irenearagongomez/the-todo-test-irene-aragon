interface Task {
    text: string; 
    complete: boolean;
}

type ToggleTask = (selectedTask: Task) => void; 

type AddTask = (text: string) => void; 