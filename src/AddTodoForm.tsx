import React, { useState } from "react";
import { alpha, styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import './style.css';


interface Props {
    addTask: AddTask;
}

const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#1e81e4',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#1e81e4',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#1e81e4',
    },
    '&:hover fieldset': {
      borderColor: '#1e81e4',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#1e81e4',
    },
  },
});

export const AddTodoForm: React.FC<Props> = ({addTask}) => {
  const [text, setText] = useState('');

  return (
    <>
    <h2 className="headline-text">A SIMPLE TODO LIST</h2>
    <form className="todo-form">
        <CssTextField  
          id="custom-css-outlined-input"
          label="Don't forget to..." 
          variant="outlined" 
          type="text" 
          value={text}
          onChange={(e) => {setText(e.target.value)}}
        />
        <Button 
          className="add-btn"
          variant="outlined"
          type="submit"
          endIcon={<AddIcon />}
          onClick={(e) => {
              e.preventDefault();
              addTask(text);
              setText('');
          }}
        >
          Add to the list
        </Button>
    </form>
    </>

  );
  
};