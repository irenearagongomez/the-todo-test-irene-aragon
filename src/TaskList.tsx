import React from "react";
import { TaskListItem } from './TaskListItem';

interface Props {
    tasks: Task[];
    toggleTask: ToggleTask;
}

export const TaskList: React.FC<Props> = ({tasks, toggleTask}) => {
    return (
        <ul>
            {tasks.map((task) => (
                <TaskListItem key={task.text} task={task} toggleTask={toggleTask} />
            ))}
        </ul>
    )
}