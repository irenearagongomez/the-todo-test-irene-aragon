import React from "react";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

interface Props {
    task: Task; 
    toggleTask: ToggleTask;
}

export const TaskListItem: React.FC<Props> = ({task, toggleTask}) => {
  return (
    <>
      <FormGroup className="container-list">
        <FormControlLabel control={
          <Checkbox 
            defaultChecked 
            checked={task.complete} 
            onClick={() => {
                toggleTask(task);
            }}
          />
        } 
          label={task.text} />
      </FormGroup>
    </>
  );
};



